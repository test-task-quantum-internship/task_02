# Solving the problem using depth-first search (DFS) algorithm:
def count_islands(matrix):
    if not matrix:
        return 0
        
    rows, cols = len(matrix), len(matrix[0])
    count = 0
    
    def dfs(row, col):
        if row < 0 or row >= rows or col < 0 or col >= cols or matrix[row][col] != 1:
            return
        matrix[row][col] = 2 # mark visited
        dfs(row+1, col)
        dfs(row-1, col)
        dfs(row, col+1)
        dfs(row, col-1)
    
    for i in range(rows):
        for j in range(cols):
            if matrix[i][j] == 1:
                dfs(i, j)
                count += 1
                
    return count


matrix_test = [
    [1, 1, 0, 0, 0],
    [1, 1, 0, 0, 1],
    [0, 0, 1, 1, 0],
    [0, 0, 0, 1, 1],
]

matrix_1 = [
    [0, 1, 0],
    [0, 0, 0],
    [0, 1, 1],
]


matrix_2 = [
    [0, 0, 0, 1],
    [0, 0, 1, 0],
    [0, 1, 0, 0],
]

matrix_3 = [
    [0, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 1, 0, 1],
]



num_islands = count_islands(matrix_3)
print(f"Number of islands: {num_islands}")  # Output: Number of islands