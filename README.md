# Task 02: Counting islands
## Task:

You have a matrix MxN that represents a map. There are 2 possible states on the map: 1 - islands, 0 - ocean. Your task is to calculate the number of islands in the most effective way. Please write code in Python 3.

Inputs:
M N
Matrix

Examples:

Input:
3 3

0 1 0
0 0 0
0 1 1

Output: 2

Input:
3 4 

0 0 0 1
0 0 1 0
0 1 0 0

Output: 3

Input:
3 4

0 0 0 1
0 0 1 1
0 1 0 1

Output: 2


## Solution:
The solution to the problem lies in using the function:
```py
def count_islands(matrix):
    if not matrix:
        return 0
        
    rows, cols = len(matrix), len(matrix[0])
    count = 0
    
    def dfs(row, col):
        if row < 0 or row >= rows or col < 0 or col >= cols or matrix[row][col] != 1:
            return
        matrix[row][col] = 2 # mark visited
        dfs(row+1, col)
        dfs(row-1, col)
        dfs(row, col+1)
        dfs(row, col-1)
    
    for i in range(rows):
        for j in range(cols):
            if matrix[i][j] == 1:
                dfs(i, j)
                count += 1
                
    return count
```

The main idea of this algorithm is to traverse the matrix using DFS starting from any unvisited 1, marking all connected 1s as visited. The count variable is incremented for each disconnected set of 1s. The time complexity of this algorithm is O(M*N), where M is the number of rows and N is the number of columns in the matrix.

If you use one of the input examples, for clarification, then the `matrix_test` represents a map with 4 rows and 5 columns, where 1s represent islands and 0s represent ocean. The `count_islands` function is called with this matrix as an argument, and the returned value is assigned to `num_islands`. Finally, the number of islands is printed to the console using an f-string. The expected output is `Number of islands: 3`, which means there are 3 islands on this map.